# Github Search Application

## Installation of project
Step 1: First create a virtual environment with command

```
python -m virtualenv env
```

Step 2: Install requirements.txt file

```
pip install -r requirements.txt
```

Step 3: Start redis server use docker for it

Step 4: Now setup frontend install dependencies from package.json

```
cd github_search_frontend
npm install
```

Step 5 : Replace github token in search/utils.py token variable mandatory for using Github API

Step 6 : Run migrate for migrations

```
python manage.py migrate
```

Step 7 : Run backend server

```
python manage.py runserver
```

Step 8 : Run frontend server

```
npm start
```


## Project documentation

Project has two API's
1. api/search/
2. api/clear-cache/

In the api/search/ API view we have two type of search Github repositories and users search

For this we have two data key user_query and repo_query in search_api view function

if user_query has data then we take it as user want to search for github users 

if repo_query has data then we take it as user want to search for github repositories

### For fetching Github search API data I have built a GithubSearchApi class which is handling two method

1. Search for repo, method is search_repo(search_query) which take search_query as parameter.
2. Search for user, method is search_user(serach_query).

### We have two challenges which are :-
1. Store the search results for particular search term in redis database and cache it
2. Don't hit Github API if we have cache data for that particular search term

### Steps to process these challenges

Step 1 : First we need to check the search type for that checking if either repo_query or user_query has data

Step 2 : If we have data in repo_query key then checking in redis cache data for key repo_ + search_term , repo is the general name use it for reference repo cache data and search_term is the query search by user

Step 3 : If we don't have data for that search term then we are hiting Github API for fetching results for search term and cache the results

Step 4: Cache the results for 2 hours

In the api/clear-cache/ api we are clearing the cache for backend
 






