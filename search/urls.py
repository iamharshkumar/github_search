from django.contrib import admin
from django.urls import path, include
from search import views

urlpatterns = [
    path('search/', views.search_api),
    path('clear-cache/', views.clear_redis_cache),
]
