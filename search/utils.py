import requests
import json

'''

Github search API has two method 
1. Search for repositories name class method is search_repo()
2. Search for users name class method is search_user()

***MANDATORY***
Add your github token in token variable

'''


class GithubSearchApi:

    api_url = 'https://api.github.com/search'
    token = ''  # add your github token here
    headers = {
        'Accept': 'application/vnd.github+json',
        'Authorization': f'Bearer {token}',
        'X-GitHub-Api-Version': '2022-11-28'
    }

    def search_repo(self, search_query):

        repo_api_url = f'{self.api_url}/repositories?q={search_query}'
        payload = {}

        response = requests.request(
            "GET", repo_api_url, headers=self.headers, data=payload)

        response = json.loads(response.text)

        repos_list = []

        for item in response['items']:
            repos_list.append({
                'full_name': item['full_name'],
                'license': item.get('license'),
                'forks': item['forks_count'],
                'language': item['language'],
                'owner': {
                    'name': item['owner']['login'],
                    'avatar_url': item['owner']['avatar_url'],
                }
            })

        return repos_list

    def search_user(self, search_query):
        repo_api_url = f'{self.api_url}/users?q={search_query}'
        payload = {}

        response = requests.request(
            "GET", repo_api_url, headers=self.headers, data=payload)

        response = json.loads(response.text)

        users_list = []

        for item in response['items']:
            users_list.append({
                'full_name': item['login'],
                'avatar': item['avatar_url'],
                'profile_url': item['html_url'],
                'repos_url': item['repos_url'],
            })

        return users_list
