from rest_framework.decorators import api_view
from rest_framework.response import Response

from search.utils import GithubSearchApi
from django_redis import get_redis_connection
from django.core.cache import cache

'''
Search for github users or repos
    Step 1: First we need to check user_query or repo_query data is None or not
    Step 2: Then Check if search term is cache in redis or not
    Step 3: If cache available then fetch results from redis and don't hit github api
    Step 4: If cache not available then hit GithubSearchApi class method search_repo or search_user 
    Step 5: Then cache the results for 2 hours in redis
'''
@api_view(['POST'])
def search_api(request):
    user_search_query = request.data.get('user_query')
    repo_search_query = request.data.get('repo_query')

    github_search_service = GithubSearchApi()

    response = []

    cache_timeout = 7200  # cache for 2 hours

    if repo_search_query:
        fetch_cache_key = cache.get(f'repo_{repo_search_query}')
        if fetch_cache_key:
            response = fetch_cache_key
        else:
            response = github_search_service.search_repo(repo_search_query)
            cache.set(f'repo_{repo_search_query}', response,
                      nx=True, timeout=cache_timeout)

    if user_search_query:
        fetch_cache_key = cache.get(f'user_{user_search_query}')
        if fetch_cache_key:
            response = fetch_cache_key
        else:
            response = github_search_service.search_user(user_search_query)
            cache.set(f'user_{user_search_query}', response,
                      nx=True, timeout=cache_timeout)

    return Response(response, status=200)


@api_view(['GET'])
def clear_redis_cache(request):
    get_redis_connection("default").flushall()
    return Response({'message': 'Cache has been cleared.'}, status=200)
