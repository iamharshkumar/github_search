import React from 'react';
import GridBox from '../../../components/gridBox';
import styled from 'styled-components';

const Wrapper = styled.section`
    display: flex;
    flex-wrap: wrap;
`

export default function DataGrid({ data, dataType }: { data: any[], dataType: string }) {

    return (
        <Wrapper>
            {
                data.map((item: any, index: number) => {
                    return (
                        <GridBox key={index} item={item} dataType={dataType} />
                    )
                })
            }
        </Wrapper>
    )
}