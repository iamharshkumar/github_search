import React from 'react';
import SearchDropdown from '../../components/searchDropdown';
import SearchInput from '../../components/searchInput';
import DataGrid from './DataGrid';
import * as API from '../../apis';
import styled from 'styled-components';
import Header from './Header';

const Wrapper = styled.section`
    ${({ active }: { active: boolean }) => active ? `
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        `: `margin: 50px;`
    }
`

const Grid = styled.section`
    display: flex; 
`

export default function Homepage() {
    const [searchType, setType] = React.useState<string>('repo');
    const [query, setQuery] = React.useState<string>('');
    const [loading, setLoading] = React.useState<boolean>(false);
    const [errorMessage, setErrorMessage] = React.useState<string | null>(null);

    const [data, setData] = React.useState<any[]>([]);

    const fetchData = () => {
        setLoading(true)
        API.searchAPI(query, searchType).then((response) => {
            setData(response.data)
            setLoading(false)

        }).catch(err => {
            setErrorMessage(err.message)
            setLoading(false)
            setData([])
        })
    }

    React.useEffect(() => {
        if (query.length >= 3) {
            fetchData()
        }
    }, [searchType])

    React.useEffect(() => {
        if (query.length >= 3) {
            const getData = setTimeout(() => {
                fetchData()
            }, 600)

            return () => clearTimeout(getData)
        } else {
            setData([])
        }
    }, [query])

    return (
        <Wrapper active={query.length < 3}>
            <Header />

            <Grid>
                <SearchInput onChange={setQuery} />
                <SearchDropdown onChange={setType} />
            </Grid>

            {
                loading ? 'Loading. . .' : <DataGrid data={data} dataType={searchType} />
            }

            {
                errorMessage && data.length == 0 ? errorMessage : ''
            }
        </Wrapper>
    )
}