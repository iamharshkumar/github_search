import React from 'react';
import styled from 'styled-components';

const Grid = styled.section`
    display: flex; 
`
const GithubLogo = styled.img`
    width:50px;
    height: 50px;
`

export default function Header() {

    return (
        <Grid>
            <div>
                <GithubLogo src='https://github.githubassets.com/images/modules/logos_page/GitHub-Mark.png'></GithubLogo>
            </div>
            <div>
                <b>Github Researcher</b>
                <p>Search users or repositories below</p>
            </div>
        </Grid>
    )
}