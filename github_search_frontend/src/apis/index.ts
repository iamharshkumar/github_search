import { SEARCH_API_URL } from "./constants"
import axios from 'axios';

export const searchAPI = async (query: string, search_type: string) => {
    const query_type = search_type == 'repo' ? 'repo_query' : 'user_query'

    let payload: any = {}
    payload[query_type] = query
    
    return await axios.post(SEARCH_API_URL, payload)
    
}