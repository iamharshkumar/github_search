import React from 'react';
import styled from 'styled-components';

const Title = styled.h3`
    text-align: center;
    color: palevioletred;
`

const Image = styled.img`
    width: 10%;
    height: 10%;
`

const Wrapper = styled.section`
    margin: 20px 20px 20px 0;
    width: 400px;
    padding: 10px 10px 20px 10px;
    border: 1px solid #BFBFBF;
    background-color: white;
    box-shadow: 5px 5px 10px #aaaaaa;
`

export default function GridBox({ item, dataType }: { item: any, dataType: string }) {

    const userBox = () => {
        return (
            <div style={{ display: 'flex' }}>
                <Image src={item.avatar}></Image>
                <div>
                    <Title><b><a target='_blank' href={item.profile_url}>{item.full_name}</a></b></Title>
                    <a target='_blank' href={item.repos_url}>Github Repository</a>
                </div>
            </div>
        )
    }

    const repoBox = () => {
        return (
            <>
                <Title>{item.full_name}</Title>
                <p>forks: {item.forks}</p>
                <p>language: {item.language}</p>
                <p>owner: {item.owner?.name}</p>
                <p>license: {item.license?.name}</p>
            </>
        )
    }

    return (
        <Wrapper>
            {
                dataType == 'repo' ? repoBox() : userBox()
            }
        </Wrapper>
    )
}