import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.section`
    padding: 10px;
`

const Select = styled.select`
    padding: 5px;
`

export default function SearchDropdown({ onChange }: { onChange: any }) {

    const onChangeDropdown = (e: React.ChangeEvent<HTMLSelectElement>) => {
        onChange(e.target.value)
    }

    return (
        <Wrapper>
            <Select onChange={onChangeDropdown} name="Github search" id="github_search">
                <option value="repo">Repos</option>
                <option value="user">Users</option>
            </Select>
        </Wrapper>
    )
}