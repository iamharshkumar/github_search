import React from 'react';
import styled from 'styled-components';

export const Wrapper = styled.section`
    padding: 10px
`

export const Input = styled.input`
    padding: 5px;
    width: 20rem;
`

export default function SearchInput({ onChange }: { onChange: any }) {

    const onChangeStatus = (e: React.ChangeEvent<HTMLInputElement>) => {
        onChange(e.target.value)
    }

    return (
        <Wrapper>
            <Input onChange={onChangeStatus} placeholder="Search repos or users..."></Input>
        </Wrapper>
    )
}